# Gallery Plugin

The **gallery**-plugin is an extension for [Grav CMS](http://github.com/getgrav/grav) that provides a thin wrapper for [lightgallery.js](https://sachinchoolur.github.io/lightgallery.js/), a "modern mobile touch slider with hardware accelerated transitions and amazing native behavior".

## Installation

Installing the Gallery-plugin can be done in one of three ways: The GPM (Grav Package Manager) installation method lets you quickly install the plugin with a simple terminal command, the manual method lets you do so via a zip file, and the admin method lets you do so via the Admin Plugin.

### GPM Installation (Preferred)

To install the plugin via the [GPM](http://learn.getgrav.org/advanced/grav-gpm), through your system's terminal (also called the command line), navigate to the root of your Grav-installation, and enter:

    bin/gpm install gallery

This will install the Gallery-plugin into your `/user/plugins`-directory within Grav. Its files can be found under `/your/site/grav/user/plugins/gallery`.

### Manual Installation

To install the plugin manually, download the zip-version of this repository and unzip it under `/your/site/grav/user/plugins`. Then rename the folder to `gallery`. You can find these files on [GitLab](https://gitlab.com/louison.chevalier/grav-plugin-gallery).

You should now have all the plugin files under

    /your/site/grav/user/plugins/gallery

### Example

```markdown
[gallery]

![Image 1](image1.jpg)

![Image 2](image2.jpg)

![Image 3](image3.jpg)

[/gallery]
```
