(function($){
    $(function(){
        $('body').on('grav-editor-ready', function() {
            var Instance = Grav.default.Forms.Fields.EditorField.Instance;
            Instance.addButton({
                gallery: {
                    identifier: 'gallery-photo',
                    title: 'Gallery',
                    label: '<i class="fa fa-file-image-o"></i>',
                    modes: ['gfm', 'markdown'],
                    action: function(_ref) {
                        var codemirror = _ref.codemirror, button = _ref.button, textarea = _ref.textarea;
                        button.on('click.editor.gallery',function() {

                            //Add text to the editor
                            var pos     = codemirror.getDoc().getCursor(true);
                            var posend  = codemirror.getDoc().getCursor(false);

                            for (var i=pos.line; i<(posend.line+1);i++) {
                                codemirror.replaceRange("[gallery][/gallery]", { line: i, ch: 0 }, { line: i, ch: codemirror.getLine(i).length });
                            }

                            codemirror.setCursor({ line: posend.line, ch: codemirror.getLine(posend.line).length-10 });
                            codemirror.focus();
                        });
                    }
                }
            });
        });
    });
})(jQuery);
