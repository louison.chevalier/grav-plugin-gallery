<?php
/**
 * Gallery Plugin
 *
 * PHP version 7
 *
 * @category   Extensions
 * @package    Grav
 * @subpackage lightgallery.js
 * @author     Louison Chevalier
 * @license    http://www.opensource.org/licenses/mit-license.html MIT License
 * @link       https://gitlab.com/louison.chevalier/grav-plugin-gallery
 */
namespace Grav\Plugin\Shortcodes;

use Grav\Common\Grav;
use Grav\Common\Utils;
use Thunder\Shortcode\Shortcode\ShortcodeInterface;

/**
 * Create a sidenote for Tufte CSS
 *
 * Class ScholarTheme
 *
 * @category Extensions
 * @package  Grav\Plugin
 * @author     Louison CHevalier
 * @license    http://www.opensource.org/licenses/mit-license.html MIT License
 * @link       https://gitlab.com/louison.chevalier/grav-plugin-gallery
 */
class GalleryShortcode extends Shortcode
{
    /**
     * Initialize shortcode
     *
     * @return string
     */
    public function init()
    {
        $this->shortcode->getHandlers()->add(
            'gallery',
            function (ShortcodeInterface $sc) {
                return $this->slideRenderer($sc, 'gallery');
            }
        );
    }

    /**
     * Render a sidenote in HTML
     *
     * @param ShortcodeInterface $sc   Accessor to Thunder\Shortcode
     * @param string             $type Type of note
     *
     * @return string
     */
    public function slideRenderer(ShortcodeInterface $sc, string $type)
    {
        $content = $sc->getParameter('content', $sc->getContent());
        $parameters = Utils::arrayUnflattenDotNotation($sc->getParameters());


        $id = Utils::generateRandomString(24);
        $output = $this->twig->processTemplate(
            'partials/gallery.html.twig',
            [
                'id' => $id,
                'content' => self::rewrap($content, $parameters)
            ]
        );
        $this->shortcode->addAssets(
            'inlineJs',
            '$(document).ready(function(){ $("#' . $id . '").lightGallery(); });'
        );

        return $output;
    }

    /**
     * Replace paragraphs with <a href=""></a>
     *
     * @param string $content Content to process
     *
     * @return string Processed content
     */
    public static function rewrap(string $content, array $parameters): string
    {
        preg_match_all( '@src="([^"]+)"@' , $content, $match );
        preg_match_all( '@alt="([^"]+)"@' , $content, $alt );

        $html = "";
        $count = count($match[1]);

        /*
           for ($i = 0; $i < $count; $i++) {
                 $html .= '<a class="pure-u-1-2 pure-u-md-1-3" href="'.$match[1][$i].'"><img';
                 if($alt[0][$i]){
                     $html .= ' alt="'.$alt[1][$i].'"';
                 }
                 $html .= ' src="'.$match[1][$i].'" /></a>';
           }
        */


        if(isset($parameters['type'])){

        }
        else{
            //DEFAULT
            if(isset($parameters['col'])){
                $class = $parameters['col'];
            }
            else{
                if($count <= 3){
                    $class = $count;
                }
                else{
                    $class = 3;
                }
            }

            for ($i = 0; $i < $count; $i++) {
                if(isset($match[1][$i])){

                    $html .= '<a';

                    if(isset($alt[1][$i])){
                        $html .= ' title="'.$alt[1][$i].'" ';
                    }

                    $html .= ' class="pure-u-1 pure-u-md-1-'.$class.'" href="'.$match[1][$i].'">';
                    $html .= '<div class="bg-gallery" style="background-image: url('.$match[1][$i].');"></div>';
                    $html .= '</a>';
                }
            }
        }

        return $html;
    }
}
